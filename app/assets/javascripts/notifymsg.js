function display_message(title, text, type)
{
	 new PNotify({
          title: title,
          text: text,
          type: type,
          styling: 'fontawesome',
          animate: {
              animate: true,
              in_class: 'zoomInLeft',
              out_class: 'zoomOutRight'
            }});
}


function display_confirm()
{
	// JRQ3883404211
	//Override the default confirm dialog by rails
        $.rails.allowAction = function(link){
          if (link.data("confirm") == undefined){
            return true;
          }
          $.rails.showConfirmationDialog(link);
          return false;
        }
        //User click confirm button
        $.rails.confirmed = function(link){
          link.data("confirm", null);
          link.trigger("click.rails");
        }
        //Display the confirmation dialog
        $.rails.showConfirmationDialog = function(link){
          var text = link.data("confirm");
          create_confirmation(text, link);
        }
}



function create_confirmation(text, link){
	(new PNotify({
		title: 'Confirmar',
        text: text,
        styling: 'fontawesome',
        type: 'info',
        icon: 'fa fa-question',
        animate: {
              animate: true,
              in_class: 'zoomInLeft',
              out_class: 'zoomOutRight'
        },
        hide: false,
        confirm: {
        	confirm: true,
        	buttons: [{
        	        	text: 'Aceptar',
        	            addClass: 'btn-success',
        	            click: function() {
        	                $.rails.confirmed(link);
        	            }},
        	            {
    	            	text: 'Cancelar',
        	            addClass: 'btn-danger',
						click: function() {
        	                PNotify.removeAll();
        	                undoModal();
        	            }}
        	            ]
        },
        buttons: {
        	closer: false,
            sticker: false
        },
        history: {
        	history: false
        },
        addclass: 'stack-modal',
        stack: {'dir1': 'down', 'dir2': 'right', 'modal': true},
        }));
}


function undoModal() {
    var elements = document.querySelectorAll('body > *');
    if (elements[0].outerHTML == '<div class="ui-pnotify-modal-overlay" style="display: block;"></div>') {
        elements[0].remove();
    }
}