class Article < ApplicationRecord
	belongs_to :article_type

	validates :article_type, presence: true
	validates :name, presence:true, length: { minimum: 10}
end
