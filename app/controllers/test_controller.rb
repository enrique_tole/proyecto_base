class TestController < ApplicationController
  before_action :set_article, only: [:show, :edit, :update, :destroy]
  before_action :set_lst_article_types, only: [:new, :create, :edit]
  
  def new
  	@article = Article.new
  end

  def create
  	@article = Article.new(article_params)

    respond_to do |format|
	    if @article.save
	        flash[:success] = "El Artículo fue creado exitosamente."
	        format.html { redirect_to articles_path }
	        format.json { render :index, status: :created, location: articles_path }
	    else
	        format.html { render :new }
	        format.json { render json: @article.errors, status: :unprocessable_entity }
	    end
	end
	end

	private
		def set_article
	      @article = Article.find(params[:id])
	    end

	    def set_lst_article_types
	      @lst_article_types = ArticleType.all
	    end

	    # Never trust parameters from the scary internet, only allow the white list through.
	    def article_params
	      params.require(:article).permit(:name, :date, :count, :article_type_id)
	    end
end
