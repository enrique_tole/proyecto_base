class ArticleTypeController < ApplicationController
	def get_json
		@article_type = ArticleType.find(params[:id])
		render json: @article_type
	end
end