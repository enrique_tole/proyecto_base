class ArticlesController < ApplicationController
  before_action :set_article, only: [:show, :edit, :update, :destroy]
  before_action :set_lst_article_types, only: [:new, :create, :edit]

  # GET /articles
  # GET /articles.json
  def index
    @articles = Article.all
  end

  # GET /articles/1
  # GET /articles/1.json
  def show
  end

  # GET /articles/new
  def new
    @article = Article.new
    #@lst_article_types = ArticleType.all
  end

  # GET /articles/1/edit
  def edit
    render layout: "modal_layout"
  end

  # POST /articles
  # POST /articles.json
  def create
    @article = Article.new(article_params)

    respond_to do |format|
      if @article.save
        flash[:success] = "El Artículo fue creado exitosamente."
        format.html { redirect_to articles_path }
        format.json { render :index, status: :created, location: articles_path }
      else
        format.html { render :new }
        format.json { render json: @article.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /articles/1
  # PATCH/PUT /articles/1.json
  def update
    respond_to do |format|
      if @article.update(article_params)
        flash[:success] = "El Artículo fue actualizado exitosamente."

# render js: "alert('Hello Rails');"
        format.html { render "articles/closemodal", layout: "modal_layout" }
        format.json { render :index, status: :ok, location: articles_path }
      else
        format.html { render :edit }
        format.json { render json: @article.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /articles/1
  # DELETE /articles/1.json
  def destroy
    @article.destroy
    respond_to do |format|
      flash[:success] = "El Artículo fue eliminado exitosamente."
      format.html { redirect_to articles_path }
      format.json { head :no_content }
    end
  end

  # def open_url
  #   @url = params[:url]
  # end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_article
      @article = Article.find(params[:id])
    end

    def set_lst_article_types
      @lst_article_types = ArticleType.all
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def article_params
      params.require(:article).permit(:name, :date, :count, :article_type_id)
    end
end
