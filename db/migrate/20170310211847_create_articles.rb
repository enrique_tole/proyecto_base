class CreateArticles < ActiveRecord::Migration[5.0]
  def change
    create_table :articles do |t|
      t.text :name
      t.text :date
      t.integer :count

      t.timestamps
    end
  end
end
