Rails.application.routes.draw do
  resources :articles
  get 'welcome/index'

  get 'test/new' => 'test#new'
  post 'test' => 'test#create'

  get 'articleType/:id/get_json' => 'article_type#get_json'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  #controlador RESTFUL (rutas y acciones definidiadas)
  #except: [:delete]
  #only: [:delete]

  #es lo mismo qué
  	# get "/articles" 			index
  	# post "/articles" 			create
  	# delete "/articles:id"  destroy
  	# get "articles/:id" 		show
  	# get "/articles/new" 		new
  	# get "/articles/:id/edit" 	edit
  	# patch "/articles/:id" 	update
  	# put "/articles/:id"  		update

  	#Dirección de inicio del sistema
  	root 'welcome#index'
end
